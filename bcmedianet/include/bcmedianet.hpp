#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/transaction.hpp>

using namespace eosio;

CONTRACT bcmedianet : public contract {
   public:
      using contract::contract;

      ACTION post( name user, std::string threadData );
      ACTION comment( name user, std::string postTRXID, std::string parentTRXID, std::string commentData );
      ACTION vote( name user, std::string postTRXID, int voteValue );

      using post_action = action_wrapper<"post"_n, &bcmedianet::post>;
      using comment_action = action_wrapper<"comment"_n, &bcmedianet::comment>;
      using vote_action = action_wrapper<"vote"_n, &bcmedianet::vote>;
};