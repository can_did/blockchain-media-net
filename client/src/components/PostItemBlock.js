
const React = require('react');

const PostItem = require('./PostItem');


class PostItemBlock extends React.Component {

  render() {
    let postItems = [];
    let errors = [];
    let preloader = [];

    for(let item of this.props.items)
      postItems.push(<PostItem
        key={item.id}
        data={item}
      />);

    for(let index in this.props.errors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.props.errors[ index ]}</div>);

    if(this.props.inprogress)
      preloader.push(<div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>);

    return(<div>
      <h1>{this.props.title}</h1>
      <br/>
      <div className="row">
        <div className="col-sm-12 preloader-container">
          {errors}

          <div className="post-list">
            {postItems}
          </div>

          {preloader}
        </div>

      </div>
    </div>)
  }

}

module.exports = PostItemBlock;
