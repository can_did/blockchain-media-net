
const React = require('react');
const Router = require('react-micro-router');
const Link = require('react-micro-router').Link;

const UserMenu = require('./UserMenu');

class TopMenu extends React.Component {

  searchClick(e) {
    e.preventDefault();
    Router.redirect('/search?query=' + document.getElementById('searchInput').value);
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-light bg-faded">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <Link href="/" className="navbar-brand" >MEDEOS</Link>
                <Link href="/sync"><img src="/img/ajax-loader-sm.gif" /></Link>
              </div>
              <div className="col-md-4">
                <form action="/search">
                  <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" className="form-control" placeholder="Search" name="query" id="searchInput"/>
                    <button className="input-group-addon" onClick={this.searchClick}>&#8227;</button>
                  </div>
                </form>
              </div>
              <div className="col-md-4">
                <UserMenu />
              </div>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

module.exports = TopMenu;
