
const React = require('react');
const Reflux = require('reflux');
const Link = require('react-micro-router').Link;

const Store = require('./Store');
const Actions = require('./Actions');

class UserMenu extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  signOutClick(e) {
    e.preventDefault();
    Actions.signOut();
  }

  ddCLick(e) {
    e.preventDefault();
    Actions.ddClick();
  }

  backDropClick(e) {
    e.preventDefault();
    Actions.ddBackDropClick();
  }

  linkClick(e) {
    e.preventDefault();
    Actions.linkClick(e.currentTarget.href);
  }

  render() {
    if( this.state.authorized === true ) {

      let ddClasses = 'dropdown ';
      let backdropClass= '';
      if( this.state.ddState ) {
        ddClasses += ' show';
        backdropClass = 'dropdown-backdrop';
      }

      return (<div className="text-right">
        <div className="btn-group">
          <Link className="btn btn-primary" href="/create" title="Create thread">+</Link>

          <div className={ddClasses}>
            <button className="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" onClick={this.ddCLick}>{this.state.userName}<span className="caret"></span></button>
            <div className={backdropClass} onClick={this.backDropClick}></div>
            <div className="dropdown-menu dropdown-menu-right">
              <a className="dropdown-item" onClick= {this.linkClick} href="/profile/content">My content</a>
              <a className="dropdown-item" onClick= {this.linkClick} href="/profile">Profile</a>
              <div className="dropdown-divider"></div>
              <a className="dropdown-item" href="/sign_out" onClick={this.signOutClick}>Sign out</a>
            </div>
          </div>
        </div>

      </div>)
    } else {
      return (<div className="text-right">
        <Link href="/sign_in">Sign in</Link>
        <span> | </span>
        <Link href="/registration">Register</Link>
      </div>)
    }
  }
}

module.exports = UserMenu;
