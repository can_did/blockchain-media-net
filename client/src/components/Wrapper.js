
const React = require('react');
const Link = require('react-micro-router').Link;

const TopMenu = require('./TopMenu');
const urlParse = require('../libs/urlParse');

class Wrapper extends React.Component {

  render() {
    return (
      <div>
        <TopMenu />
        <div className="container">
          <br/>
          <this.props.page queryParams={urlParse()}/>
        </div>
      </div>
    )
  }
}

module.exports = Wrapper;
