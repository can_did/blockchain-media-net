 
const Axios = require('axios');

class UserAccount {

  constructor(appConfig) {
    this.appConfig = appConfig;
    this.authorized = false;
    this.accessToken = {};
    this.user = {};
    this.blockchainInfo = {};
    this.token = localStorage.getItem('token');
    this.listeners = [];
  }

  init() {
    return new Promise( (resolve) => {
      if( this.token && (this.authorized === false) ) {
        Axios({
          url: `${this.appConfig.api.url}/auth`,
          method: 'get',
          timeout: this.appConfig.api.timeout,
          headers: {
            Authorization: 'Bearer ' + this.token
          }
        })
        .then( (result) => {
          this.authorize(result.data.data[ 0 ]);
          return resolve(true);
        })
        .catch( (err) => {
          this.unauthorize();
          return resolve(true);
        })

      } else {
        return resolve(false);
      }
    });
  }

  authorize(data) {
    this.authorized = true;
    this.accessToken = data.token;
    this.user = data.user;
    this.token = data.token.token;
    localStorage.setItem('token', data.token.token);
    this.updateListeners();
  }

  unauthorize() {
    this.authorized = false;
    this.accessToken = {};
    this.user = {};
    this.token = null;
    localStorage.removeItem('token');
    this.updateListeners();
  }

  updateListeners() {
    for(let cb of this.listeners)
      cb();
  }

  updateBlockchainInfo(info) {
    this.blockchainInfo = info;
    this.updateListeners();
  }

  subscribe(cb) {
    this.listeners.push(cb);
  }

}

module.exports = UserAccount;