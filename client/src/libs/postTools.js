
const striptags = require('striptags');

function preparePost(rawHTML) {
  let result = striptags(rawHTML);
  result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');

  result = result.replace(/\[b\]/gi, '<b>');
  result = result.replace(/\[\/b\]/gi, '</b>');
  result = result.replace(/\[i\]/gi, '<i>');
  result = result.replace(/\[\/i\]/gi, '</i>');
  result = result.replace(/\[h\]/gi, '<h3>');
  result = result.replace(/\[\/h\]/gi, '</h3>');
  result = result.replace(/\[s\]/gi, '<strike>');
  result = result.replace(/\[\/s\]/gi, '</strike>');

  return result;
}

module.exports = { preparePost } 
