
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

class MetaMenu extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  categoryChange(e) {
    Actions.fieldUpdate('postCategory', e.target.value);
  }

  langChange(e) {
    Actions.fieldUpdate('postLang', e.target.value);
  }

  tagsChange(e) {
    Actions.fieldUpdate('postTags', e.target.value);
  }

  render() {
    return (<div>

      <div className="form-group">
        <label>Category</label>
        <select className="form-control" onChange={this.categoryChange} value={this.state.postCategory} >
          <option value="movie">movie</option>
          <option value="anime">anime</option>
          <option value="transport">transport</option>
          <option value="sport">sport</option>
          <option value="travels">travels</option>
          <option value="games">games</option>
          <option value="people and blogs">people and blogs</option>
          <option value="humor">humor</option>
          <option value="entertainment">entertainment</option>
          <option value="news">news</option>
          <option value="politics">politics</option>
          <option value="hobby">hobby</option>
          <option value="style">style</option>
          <option value="politics">politics</option>
          <option value="education">education</option>
          <option value="science">science</option>
          <option value="technology">technology</option>
          <option value="social">social</option>
          <option value="sex">sex</option>
          <option value="music">music</option>
          <option value="life">life</option>
        </select>
      </div>
      <div className="form-group">
        <label>Lang</label>
        <select className="form-control" onChange={this.langChange} value={this.state.postLang} >
          <option value="EN">EN</option>
          <option value="DE">DE</option>
          <option value="FR">FR</option>
          <option value="JP">JP</option>
          <option value="PG">PG</option>
          <option value="RU">RU</option>
          <option value="SP">SP</option>
        </select>
      </div>
      <div className="form-group">
        <label>Tags</label>
        <input type="text" className="form-control" onChange={this.tagsChange} value={this.state.postTags} />
      </div>

    </div>)
  }
}

module.exports = MetaMenu;
