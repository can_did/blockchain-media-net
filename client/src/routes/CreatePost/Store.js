
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../config');

const blockTemplates = {
  text: {
    type: 'text',
    body: 'Lorem ipsum'
  },
  image: {
    type: 'image',
    body: '',
    title: '',
    align: 'center'
  }
};

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      postBlocks: [],
      postTitle: '',
      postLang: 'EN',
      postCategory: 'animals',
      postTags: '',
      editor: true,
      preview: false,
      errors: [],
      formInprogress: false
    };

    this.listenables = Actions;
  }

  onFieldUpdate(field, value) {
    let update = {};
    update[ field ] = value;
    this.setState(update);
  }

  onImageTitleUpdate(index, title) {
    let postBlocks = this.state.postBlocks;
    postBlocks[ index ].title = title;
    this.setState({ postBlocks: postBlocks });
  }

  onImagePositionUpdate(index, position) {
    let postBlocks = this.state.postBlocks;
    postBlocks[ index ].align = position;
    this.setState({ postBlocks: postBlocks });
  }

  onImageUpdate(index, data, fileName) {
    let postBlocks = this.state.postBlocks;
    postBlocks[ index ].body = data;
    postBlocks[ index ].title = fileName;
    this.setState({ postBlocks: postBlocks });
  }

  onCreateClick() {
    this.setState({ formInprogress: true });

    let formData = {
      title: this.state.postTitle,
      body: this.state.postBlocks,
      lang: this.state.postLang,
      tags: this.state.postTags,
      category: this.state.postCategory
    }

    Axios({
      url: `${config.api.url}/post`,
      method: 'post',
      data: formData,
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      }
    })
    .then( (result) => {
      this.setState({
        postBlocks: [],
        postTitle: '',
        postLang: 'EN',
        postCategory: 'animals',
        postTags: '',
        editor: true,
        preview: false,
        errors: [],
        formInprogress: false
      });
      Router.redirect('/profile/content');
    })
    .catch( (err) => {
      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }

  onTitleChange(value) {
    this.setState({ postTitle: value });
  }

  onTextBodyChange(blockIndex, value) {
    let postBlocks = this.state.postBlocks;
    postBlocks[ blockIndex ].body = value;
    this.setState({ postBlocks: postBlocks });
  }

  onDeleteBlock(deleteIndex) {
    let freshPostBlocks = [];

    for(let index in this.state.postBlocks)
      if( deleteIndex != index )
        freshPostBlocks.push(this.state.postBlocks[ index ]);

    this.setState({ postBlocks: freshPostBlocks });
  }

  onAddBlock(blockType) {
    let postBlocks = this.state.postBlocks;
    postBlocks.push(Object.assign({}, blockTemplates[ blockType ]));
    this.setState({ postBlocks: postBlocks });
  }

  onEditorClick() {
    this.setState({ editor: true, preview: false });
  }

  onPreviewClick() {
    this.setState({ editor: false, preview: true });
  }

  onDataLoad() {
    //console.log('* onDataLoad');
  }
}

module.exports = PageStore;
