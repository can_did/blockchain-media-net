
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadComments',
  'updateCommentBody',
  'updateCommentFile',
  'updateCommentParent',
  'submitComment',
  'switchCommentFormState',
  'activateAnswer',
  'commentImageUpdate'
]);

module.exports = Actions;
