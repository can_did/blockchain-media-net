
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../../config');

const commentBodyMinLength = 5;
const CommentBodyMaxLength = 65535;

function parseCommentTree(comments, parent, depth=0) {
  
  if( depth > 10 )
    return [];
  
  let result = [];
  let _comments = [];
  
  for(let item of comments)
    if( item !== null )
      _comments.push(item);

  for(let index in _comments) {
    let item = Object.assign({}, _comments[ index ]);
    
    if( item.parent_blockchain_trx_id === parent ) {
      _comments[ index ] = null;
      item.answers = parseCommentTree(_comments, item.blockchain_trx_id, depth+1);
      result.push(item);
    }
  }
  
  return result;
}

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      errors: [],
      formErrors: [],
      formInprogress: false,
      commentList: [],
      newCommentFormsState: false,
      newCommentBody: '',
      newCommentFile: '',
      newCommentFileName: '',
      newCommentFileType: '',
      newCommentParentUser: '',
      newCommentParentTRXID: '',
      newCommentParentTRXBCID: ''
    };

    this.listenables = Actions;
  }
  
  onCommentImageUpdate(src, fileName, type) {
    this.setState({
      newCommentFileName: fileName,
      newCommentFile: src,
      newCommentFileType: type
    });
  }
  
  onActivateAnswer(trxID, blockID, user) {
    this.setState({
      newCommentParentTRXID: trxID,
      newCommentParentTRXBCID: blockID,
      newCommentParentUser: user,
      newCommentFormsState: true
    });
  }
  
  onSwitchCommentFormState(state) {
    this.setState({
      newCommentFormsState: state,
      newCommentParentTRXID: '',
      newCommentParentTRXBCID: '',
      newCommentParentUser: ''
    });
  }

  onSubmitComment() {
    if( this.state.newCommentBody.length < commentBodyMinLength )
      return this.setState({ formErrors: [ 'Comment body is too small' ] });

    if( this.state.newCommentBody.length > CommentBodyMaxLength )
      return this.setState({ formErrors: [ 'Comment body is too big' ] });

    let formData = {
      body: this.state.newCommentBody,
      file: this.state.newCommentFile,
      file_name: this.state.newCommentFileName,
      file_type: this.state.newCommentFileType,
      parent_blockchain_block_id: this.state.newCommentParentTRXBCID,
      parent_blockchain_trx_id: this.state.newCommentParentTRXID,
      parent_user: this.state.newCommentParentUser
    }

    this.setState({ formInprogress: true });

    Axios({
      url: `${config.api.url}/post/${Router.getParams()[ 0 ]}/comments`,
      method: 'post',
      data: formData,
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      }
    })
    .then( (result) => {
      this.setState({
        formErrors: [],
        formInprogress: false,
        newCommentBody: '',
        newCommentFile: '',
        newCommentFileType: '',
        newCommentParentUser: '',
        newCommentParentTRXID: '',
        newCommentParentTRXBCID: ''
      });
      
      this.onLoadComments();
    })
    .catch( (err) => {
      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }

  onUpdateCommentBody(text) {
    this.setState({ newCommentBody: text });
  }

  onUpdateCommentFile(fileBody) {
    this.setState({ newCommentFile: fileBody });
  }

  onUpdateCommentParent(text) {
    this.setState({ newCommentParent: text });
  }

  onLoadComments() {
    Axios({
      url: `${config.api.url}/post/${Router.getParams()[ 0 ]}/comments`,
      method: 'get',
      timeout: config.api.timeout,
      //headers: {}
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, commentList: parseCommentTree(result.data.data, null) });
    })
    .catch( (err, data) => {

      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }
}

module.exports = PageStore;
