
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

class TopCreateCommentForm extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  commentBodyChange(e) {
    Actions.updateCommentBody(e.target.value);
  }

  replayToBodyChange(e) {
    Actions.updateCommentParent(e.target.value);
  }

  formSubmit() {
    Actions.submitComment();
  }
  
  formShow() {
    Actions.switchCommentFormState(true);
  }
  
  formHide() {
    Actions.switchCommentFormState(false);
  }
  
  fileChanged(e) {
    let reader = new FileReader();
    let fileName = e.target.files[ 0 ].name;

    reader.onload = function(e) {
      Actions.commentImageUpdate(e.srcElement.result, fileName, 'image');
    }

    reader.readAsDataURL(e.target.files[ 0 ]);
  }
  
  clearFile() {
    Actions.commentImageUpdate('', '', '');
  }
  
  openFile(e) {
    e.target.parentNode.children[ 0 ].click();
  }
  
  fileSelectClick(e) {
    if( e.target.children[ 0 ] )
      e.target.children[ 0 ].click();
  }
  
  render() {
    
    if( !this.state.newCommentFormsState ) // form is closed
      return(<div className="text-center">
        <button className="btn btn-secondary" onClick={this.formShow}>Post a comment</button>
      </div>)
    
    let errors = [];
    for(let index in this.state.formErrors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.formErrors[ index ]}</div>);
    
    let answerBlock;
    if( this.state.newCommentParentTRXID )
      answerBlock = (<div className="row">
        <div className="col-sm-5">Answer to: {this.state.newCommentParentUser}</div>
        <div className="col-sm-7">
          <input
            type="text"
            className="form-control"
            readonly={true}
            value={this.state.newCommentParentTRXBCID + ':' + this.state.newCommentParentTRXID} />
        </div>
      </div>);

    return(<div className="comment-background">
      <div className="row comment-header">
        <div className="col-sm-6">
          Post a comment
        </div>
        <div className="col-sm-6 text-right">
          <button className="btn btn-warning btn-sm" onClick={this.formHide}>X</button>
        </div>
      </div>
      {errors}
      {answerBlock}
      <textarea className="form-control" onChange={this.commentBodyChange} placeholder="Comment body" value={this.state.newCommentBody}></textarea>
      <div className="row">
        <div className="col-sm-6">
          <input type="file" className="hidden" onChange={(e) => this.fileChanged(e)} accept="image/*" />
          <button className="btn btn-secondary" onClick={this.openFile}>Open...</button>
          <span>{this.state.newCommentFileName} </span>
          <button className="btn btn-secondary" onClick={this.clearFile}>Clear</button>
        </div>
        <div className="col-sm-6 text-right">
          <button className="btn btn-primary" onClick={this.formSubmit} disabled={this.state.formInprogress}>Post a comment</button>
        </div>
      </div>
    </div>)
  }
}

module.exports = TopCreateCommentForm;
