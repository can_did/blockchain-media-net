
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');
const TopCreateCommentForm = require('./TopCreateCommentForm');

const postTools = require('../../../libs/postTools'); 

class CommentItem extends Reflux.Component {
  
  constructor(props) {
    super(props);
    this.store = Store;
  }
  
  answer() {
    Actions.activateAnswer(this.props.data.blockchain_trx_id, this.props.data.blockchain_block_id, this.props.data.blockchain_user);
  }
  
  render() {    
    let html = postTools.preparePost(this.props.data.body);
    
    let blockClass = 'comment-item';
    if( this.state.newCommentParentTRXID === this.props.data.blockchain_trx_id )
      blockClass += ' answer-to-this';
    
    let bodyClass = "comment-boody col-sm-12";
    let attachment;
    
    if( this.props.data.file && this.props.data.file_type == 'image' ) {
      bodyClass = "comment-boody col-sm-9";
      attachment = (<div className="col-sm-3">
        <img className="comment-image-preview" src={this.props.data.file} title={this.props.data.file_name} />
      </div>)
    }
    
    let answers = [];
    for(let item of this.props.data.answers)
      answers.push(<CommentItem key={item.blockchain_trx_id} data={item} />);

    return(<div className={blockClass}>
      <div className="comment-header row">
        <div className="col-sm-6">
          <span className="comment-author"><strong>By</strong>{this.props.data.blockchain_user}</span>
          <span className="comment-date">{this.props.data.createdAt}</span>
        </div>
        <div className="col-sm-6 text-right">
          <button className="btn btn-secondary btn-sm" onClick={()=>this.answer()}>Answer</button>
        </div>
      </div>
      <div className="row">
        {attachment}
        <div className={bodyClass} dangerouslySetInnerHTML={{ __html: html }}></div>
      </div>
      <div className="comment-answers">{answers}</div>
    </div>)
  }
}

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadComments();
  }

  render() {
    if( this.state.errors.length > 0 ) {
      let errors = [];
      for(let index in this.state.errors)
        errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.errors[ index ]}</div>);

      return(<div>
        {errors}
      </div>)
    }

    let comments = [];

    for(let item of this.state.commentList)
      comments.push(<CommentItem key={item.blockchain_trx_id} data={item} />);

    return (<div>
      <hr/>
      <TopCreateCommentForm/>
      <hr/>
      {comments}
    </div>)
  }
}

module.exports = Page;
