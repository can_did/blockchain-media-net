
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../config');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      errors: [],
      formInprogress: false,
      postData: {
        body: "[]"
      }
    };

    this.listenables = Actions;
  }

  onLoadPost() {
    Axios({
      url: `${config.api.url}/post/${Router.getParams()[ 0 ]}`,
      method: 'get',
      timeout: config.api.timeout,
      //headers: {}
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, postData: result.data.data[ 0 ] });
    })
    .catch( (err, data) => {

      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }
}

module.exports = PageStore;
