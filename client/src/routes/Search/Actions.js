
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadSearchPost',
]);

module.exports = Actions;
