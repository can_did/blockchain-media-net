
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const PostItemBlock = require('../../components/PostItemBlock');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    if( this.props.queryParams.query ) {
      Actions.loadSearchPost(this.props.queryParams.query);
    }
  }

  render() {
    if( this.props.queryParams.query ) {
      Actions.loadSearchPost(this.props.queryParams.query);
    }

    return (<div>
      <PostItemBlock
        title="Search results"
        items={this.state.freshPosts}
        errors={this.state.freshPostsErrors}
        inprogress={this.state.freshPostsInprogress}
      />
    </div>)
  }
}

module.exports = Page;
