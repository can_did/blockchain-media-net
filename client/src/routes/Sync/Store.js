
const Axios = require('axios');
const Reflux = require('reflux');


const Actions = require('./Actions');
const config = require('../../../config');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      syncErrors: [],
      syncInprogress: false,
      chainInfo: {}
    };

    this.listenables = Actions;
  }

  onLoadChainInfo() {

    this.setState({ syncInprogress: true });

    Axios({
      url: `${config.api.url}/sync`,
      method: 'get',
      timeout: config.api.timeout
    })
    .then( (result) => {
      this.setState({ syncErrors: [], syncInprogress: false, chainInfo: result.data.data[0] });
    })
    .catch( (err, data) => {
      if( err.response && err.response.data )
        return this.setState({ syncErrors: [ err.response.data.meta.error.message ], syncInprogress: false });

      return this.setState({ syncErrors: [ err ], syncInprogress: false });
    });
  }
}

module.exports = PageStore;
