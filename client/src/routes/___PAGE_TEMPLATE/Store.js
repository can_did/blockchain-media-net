
const Reflux = require('reflux');

const Actions = require('./Actions');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      pageData: {}
    };

    this.listenables = Actions;
  }

  onDataLoad() {
    console.log('* onDataLoad');
  }
}

module.exports = PageStore;