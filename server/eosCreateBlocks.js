const config = require('./config');
const Logger = require('./src/libs/Logger');
const modelsLoader = require('./src/libs/modelsLoader');
const create_eos_blocks = require('./src/tasks/create_eos_blocks');

async function main() {

  const logger = new Logger(config.logLVL, 'masternode-eos-block-creator');
  const db = await modelsLoader(config.db);

  logger.log('info', 'app-message', 'applications has started');

  setInterval(async function() {
    let result = await create_eos_blocks(db, config, logger);
    logger.log('info', 'task-results', `CREATED: ${result}`);
  }, 10000);
}

main();
