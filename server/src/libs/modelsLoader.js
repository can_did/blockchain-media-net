const Sequelize = require('sequelize');
const fs = require('fs');

function getModelFilespath(path) {
  return new Promise( function(resolve, reject) {    
    fs.readdir(path, {}, function(err, files) {
      if( err )
        return reject(err);
      
      return resolve(files);
    });
  });
}

async function loadModels(dbConfig) {
  let sequelize = new Sequelize(dbConfig);

  let modelFiles = await getModelFilespath('./src/models/');

  for(let fileName of modelFiles) {
    const patt = /\.js$/gi;
    if( patt.exec(fileName) ) {
      require(`../models/${fileName}`)(sequelize, Sequelize.DataTypes);
    }
  }
  return sequelize;
}

module.exports = loadModels;
