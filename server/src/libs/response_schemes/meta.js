
const Joi = require('joi');

const schema = Joi.object({
  total: Joi.number().integer().required().description('The total number of pages for the given query executed.'),
  count: Joi.number().integer().required().description('The total number of pages for the given query executed on the page.'),
  offset: Joi.number().integer().required().description('The offset for the given query executed on the page.'),

  error: Joi.object({
    message: Joi.string(), details: Joi.string(),
  }).allow(null).optionalKeys('message', 'details').description('If error happen it will contain error message.'),
});

module.exports = schema;
