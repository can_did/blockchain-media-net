const Joi = require('joi');

const syncSchema = Joi.object({
  INIT: Joi.number().integer(),
  INPROGRESS: Joi.number().integer(),
  DONE: Joi.number().integer(),
  ERROR: Joi.number().integer()
});

module.exports = syncSchema;
