'use strict';

const voteTypes = {
  like: { id: 1, name: 'like' },
  dislike: { id: 0, name: 'dislike' }
};

module.exports = (sequelize, DataTypes) => {
  const comment_votes = sequelize.define('comment_votes', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    blockchain_user: {
      type: DataTypes.STRING
    },
    blockchain_trx_id: {
      type: DataTypes.STRING
    },
    blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    comment_blockchain_block_id: {
      type: DataTypes.STRING
    },
    comment_blockchain_trx_id: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.INTEGER
    },
  });

  comment_votes.voteTypes = voteTypes;

  comment_votes.dummyData = [];

  return comment_votes;
};
