'use strict';

module.exports = (sequelize, DataTypes) => {
  const comments = sequelize.define('comments', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    blockchain_user: {
      type: DataTypes.STRING
    },
    blockchain_trx_id: {
      type: DataTypes.STRING
    },
    blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    post_blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    post_blockchain_trx_id: {
      type: DataTypes.STRING
    },
    parent_blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    parent_blockchain_trx_id: {
      type: DataTypes.STRING
    },
    body: {
      type: DataTypes.TEXT('long')
    },
    file: {
      type: DataTypes.TEXT('long')
    },
    file_name: {
      type: DataTypes.TEXT('long')
    },
    file_type: {
      type: DataTypes.STRING
    },
  });

  comments.dummyData = [];

  return comments;
};
