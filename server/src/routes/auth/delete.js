
const Joi = require('joi');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../libs/response_schemes/meta');
const okSchema = require('../../libs/response_schemes/okSchema');

async function response(request) {

  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');

  await accessTokens.destroy({ where: { token: request.auth.artifacts.token } });

  return buildResult([ { result: 'ok' } ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(okSchema)
});

module.exports = {
  method: 'DELETE',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Delete token',
    notes: 'Delete token',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true })
    },
    response: { schema: responseSchema }
  }
};
