
const Joi = require('joi');
const Boom = require('boom');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../libs/response_schemes/meta');
const accessTokenSchema = require('../../libs/response_schemes/accessToken');
const userSchema = require('../../libs/response_schemes/user');

async function response(request) {

  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');
  
  let userRecord = await users.findOne({ where: { email: request.payload.email } });

  if( !userRecord ) {
    throw Boom.unauthorized('Wrong email or password');
  }
  
  if( !users.verifyPassword(request.payload.password, userRecord.password) ) {
    throw Boom.unauthorized('Wrong email or password');
  }
  
  let token = await accessTokens.createAccessToken(userRecord);
  let exportUser = {
    id: userRecord.id,
    email: userRecord.email,
    eos_name: userRecord.eos_name,
    eos_public: userRecord.eos_public,
    updatedAt: userRecord.updatedAt,
    createdAt: userRecord.createdAt,
  };

  return buildResult([ { token: token.dataValues, user: exportUser } ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items({
    token: accessTokenSchema,
    user: userSchema
  })
});

module.exports = {
  method: 'POST',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'User authorization',
    notes: 'User authorization',
    tags: [ 'api' ],
    validate: {
      payload: {
        email: Joi.string().required().example('pupkin@gmail.com'),
        password: Joi.string().required().example('12345')
      }
    },
    response: { schema: responseSchema }
  }
};
