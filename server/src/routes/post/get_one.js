
const Joi = require('joi');
const Boom = require('boom');

const metaSchema = require('../../libs/response_schemes/meta');
const buildResult = require('../../libs/controllersHelpers').buildResult;
const postSchema = require('../../libs/response_schemes/post');

async function response(request) {

  const posts = request.getModel(request.server.config.db.database, 'posts');
  let paramsParts = request.params.id.split(':');

  let blockID = parseInt(paramsParts[ 0 ]);
  let trxID = paramsParts[ 1 ];

  let resultPost = await posts.findOne({ where: { blockchain_trx_id: trxID, blockchain_block_id: blockID } });

  if( !resultPost ) {
    throw Boom.notFound('Post not found');
  }

  return buildResult([ resultPost.get({ plain: true }) ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(postSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/post/{id}',
  options: {
    handler: response,
    description: 'Get post by blockID:trx_ID',
    notes: 'Get post by blockID:trx_ID',
    tags: [ 'api' ],
    auth: false,
    validate: {
      params: {
        id: Joi.string().description('blockID:trx_ID')
          .example('28610416:be0a0734a0b785398243e5710f5db98a072dd4bf00e6ecbfbc7cfcd91a743014')
      }
    },
    response: { schema: responseSchema }
  }
};
