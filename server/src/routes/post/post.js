
const Joi = require('joi');
const Boom = require('boom');

const metaSchema = require('../../libs/response_schemes/meta');
const buildResult = require('../../libs/controllersHelpers').buildResult;
const postSchema = require('../../libs/response_schemes/post');
const BlockChainConnector = require('../../libs/BlockChainConnector');

async function response(request) {

  let connector = new BlockChainConnector(request.server.config.blockchain);
  const posts = request.getModel(request.server.config.db.database, 'posts');

  let BCUser = {
    name: request.auth.artifacts.user.eos_name,
    privateKey: request.auth.artifacts.user.eos_private,
    publicKey: request.auth.artifacts.user.eos_public,
  };

  let BCResult;

  try {
    BCResult = await connector.post(JSON.stringify(request.payload), BCUser);
  } catch(err) {
    throw Boom.unauthorized(err);
  }
  
  console.log('*** BCResult::', JSON.stringify(BCResult, null, '  '));

  let newPost = {
    blockchain_user: BCUser.name,
    blockchain_trx_id: BCResult.transaction_id,
    blockchain_block_id: BCResult.processed.block_num,
    title: request.payload.title,
    body: JSON.stringify(request.payload.body),
    lang: request.payload.lang,
    tags: request.payload.tags,
    type: '',
    category: request.payload.category,
    createdAt: BCResult.processed.block_time,
    updatedAt: BCResult.processed.block_time
  };
  
  let newDBPost = await posts.create(newPost);

  return buildResult([ newDBPost.dataValues ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(postSchema)
});

module.exports = {
  method: 'POST',
  path: '/api/post',
  options: {
    handler: response,
    description: 'Token confirmation',
    notes: 'Token confirmation',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true }),
      payload: {
        body: Joi.array(),
        category: Joi.string().min(3).max(24).description('Post category').example('animals'),
        lang: Joi.string().min(2).max(2).description('Post language, 2 chars symbol').example('EN'),
        tags: Joi.string().allow([null, '']).description('Post tags divided by space').example('ololo ufo_driver'),
        title: Joi.string().min(5).max(256).description('Post title').example('My new post')
      }
    },
    response: { schema: responseSchema }
  }
};
