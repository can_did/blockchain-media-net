
const Joi = require('joi');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const syncSchema = require('../../libs/response_schemes/sync.js');
const metaSchema = require('../../libs/response_schemes/meta');

async function response(request) {

  const blocks = request.getModel(request.server.config.db.database, 'blocks');
  
  let result = {};
  
  for(let status in blocks.statuses)
    result[ status ] = await blocks.count({ where: { status: blocks.statuses[ status ].id } });
  
  return buildResult([result], 0, 0, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(syncSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/sync',
  options: {
    handler: response,
    description: 'Blockchain sync status',
    notes: 'Blockchain sync status',
    tags: ['api', 'web'], // Necessary tag for swagger
    auth: false,
    validate: {
    },

    response: { schema: responseSchema }
  }
};
