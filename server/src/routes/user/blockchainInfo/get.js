
const Joi = require('joi');

const metaSchema = require('../../../libs/response_schemes/meta');
const buildResult = require('../../../libs/controllersHelpers').buildResult;
const BlockChainConnector = require('../../../libs/BlockChainConnector');
const blockchainInfoSchema = require('../../../libs/response_schemes/blockchainInfo');

async function response(request) {

  let connector = new BlockChainConnector(request.server.config.blockchain);
  let info = await connector.userInfo(request.auth.artifacts.user.eos_name);

  return buildResult([ info ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(blockchainInfoSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/user/blockchainInfo',
  options: {
    handler: response,
    description: 'Get blockchain information for current user',
    notes: 'Get blockchain information for current user',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true })
    },
    response: { schema: responseSchema }
  }
};
