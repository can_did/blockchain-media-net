
const Joi = require('joi');
const Boom = require('boom');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../libs/response_schemes/meta');
const accessTokenSchema = require('../../libs/response_schemes/accessToken');
const userSchema = require('../../libs/response_schemes/user');

const MIN_PASS_LENGTH = 3;

function checkForm(payload) {
  if( payload.password.length < MIN_PASS_LENGTH )
    return 'Password is too short';

  if( payload.password !== payload.confirmation )
    return 'Password and confirmation do not match';

  if( payload.email.length < 3 ) // hardcode
    return 'Wrong email';

  return false;
}

async function response(request) {

  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');
  
  let userRecord = await users.findOne({ where: { email: request.payload.email } });

  if( userRecord ) {
    throw Boom.badRequest('Email already used');
  }

  let validationResult = checkForm(request.payload);
  if( validationResult !== false ) {
    throw Boom.badRequest(validationResult);
  }

  let newRecord = {
    email: request.payload.email,
    password: users.hashPassword(request.payload.password),
    eos_name: request.payload.eos_name || '',
    eos_private: request.payload.eos_private || '',
    eos_public: request.payload.eos_public || '',
  };
  let newUser = await users.create(newRecord);
  
  let token = await accessTokens.createAccessToken(newUser);
  let exportUser = {
    id: newUser.id,
    email: newUser.email,
    eos_name: newUser.eos_name,
    eos_public: newUser.eos_public,
    updatedAt: newUser.updatedAt,
    createdAt: newUser.createdAt,
  };

  return buildResult([ { token: token.dataValues, user: exportUser } ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items({
    token: accessTokenSchema,
    user: userSchema
  })
});

module.exports = {
  method: 'POST',
  path: '/api/user',
  options: {
    handler: response,
    description: 'User authorization',
    notes: 'User authorization',
    tags: [ 'api' ],
    validate: {
      payload: {
        email: Joi.string().required().example('pupkin@gmail.com'),
        password: Joi.string().required().example('12345'),
        confirmation: Joi.string().required().example('12345'),
        eos_name: Joi.string().allow(['', null]).example('pupkin.eosio'),
        eos_private: Joi.string().allow(['', null]).example('5JGwstdCwyiEh7pCfPGcRdqFcR8ZW1b2cpTjwE8TWRF15Xz3BFb'),
        eos_public: Joi.string().allow(['', null]).example('EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s')
      }
    },
    response: { schema: responseSchema }
  }
};
