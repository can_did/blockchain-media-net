
const Joi = require('joi');

const buildResult = require('../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../libs/response_schemes/meta');
const userSchema = require('../../libs/response_schemes/user');

async function response(request) {

  const users = request.getModel(request.server.config.db.database, 'users');

  let updatedUser = await users.update(request.payload, { where: { id: request.auth.artifacts.user.id } });

  let exportUser = {
    id: updatedUser.id,
    email: updatedUser.email,
    eos_name: updatedUser.eos_name,
    eos_public: updatedUser.eos_public,
    eos_private: updatedUser.eos_private,
    createdAt: updatedUser.createdAt,
    updatedAt: updatedUser.updatedAt,
  };

  return buildResult([ exportUser ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(userSchema)
});

module.exports = {
  method: 'PUT',
  path: '/api/user',
  options: {
    handler: response,
    description: 'Update EOS profile',
    notes: 'Update EOS profile',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      payload: {
        eos_name: Joi.string().allow(['', null]).example('pupkin.eosio'),
        eos_private: Joi.string().allow(['', null]).example('5JGwstdCwyiEh7pCfPGcRdqFcR8ZW1b2cpTjwE8TWRF15Xz3BFb'),
        eos_public: Joi.string().allow(['', null]).example('EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s')
      }
    },
    response: { schema: responseSchema }
  }
};
